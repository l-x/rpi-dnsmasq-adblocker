FROM arm32v6/alpine:latest

RUN \
	apk update && \
	apk add dnsmasq curl && \
	mkdir /datadir

ADD https://raw.githubusercontent.com/notracking/hosts-blocklists/master/hostnames.txt /datadir/hostnames.txt
ADD https://raw.githubusercontent.com/notracking/hosts-blocklists/master/domains.txt /datadir/domains.txt
ADD ./entrypoint /entrypoint
ADD ./update /usr/bin/update

VOLUME /datadir

CMD [ "dnsmasq","-d", "-H" ,"/datadir/hostnames.txt", "-C", "/datadir/domains.txt", "-S", "8.8.8.8", "-S", "8.8.4.4"]
ENTRYPOINT ["/entrypoint"]